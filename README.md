# React Simple App

Just a simple application implemented using React.

## Table of contents

1. [Introduction](#markdown-header-introduction)
2. [Prerequisites](#markdown-header-prerequisites)
3. [Creating the Application](#markdown-header-creating-the-application)
4. [Running the Application](#markdown-header-running-the-application)
5. [Dynamic Usage of Components](#markdown-header-dynamic-usage-of-components)
6. [Storage](#markdown-header-storage)

## Introduction

This repository will guide you through the process of creating and setting up a new React application.

It is highly recommended that you follow the [Getting Started](https://reactjs.org/docs/getting-started.html) guide on the official website.

**Note:** This repository already provides a partially implemented RESTful calculator.
If you want to skip the setup and/or will be using the provided app, please skip to section [**Running the application**](#markdown-header-running-the-application).

## Prerequisites

Before you begin, make sure your development environment includes [Node.js®](https://nodejs.org/) and an npm package manager (should come with Node.js already).

Since the provided application is a RESTful service, please make sure the [**server**](https://bitbucket.org/pmpalmeida/simplespringrestserver/) is running while testing the application.

### Node.js

React requires Node.js version 8.10 or later.

* Run `node -v` in a terminal to check your version.

## Creating the application

We will use the `create-react-app` command to setup the development environment.

* Run the command `create-react-app` and provide the name `my-app`, as show:
```
npx create-react-app new my-app
```

**Note:** `npx` on the first line is not a typo — it’s a package runner tool that comes with npm 5.2+.

Create React App doesn’t handle backend logic or databases; it just creates a frontend build pipeline, so you can use it with any backend you want. Under the hood, it uses Babel and webpack.

## Running the application

1. Go to the workspace folder.
2. Launch the server by using the command `npm start`.

```
cd my-app
npm start
```

The `npm start` runs the application in development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Dynamic Usage of Components

Dynamism in React boils down to pre-creating all the chunks you will ever need. Then you just need a way to dynamically refer to them.

Here is an [example](https://stackoverflow.com/a/40330401/6210860).

It is possible to inject Inner HTML from server-side but is **not recommended** and very risky! It is very susceptible to injection of malicious code.

Also check out:

* [Loadables](https://github.com/jamiebuilds/react-loadable)

## Storage

Base React only provides support for WebStorage ([Local Storage and Session Storage](https://javascript.info/localstorage)).

It is possible however, to use Cookies as another mean of Storage (check [React-Cookie](https://www.npmjs.com/package/react-cookie)).

This war between which one is best has been going on for years. Here's a quick [review](https://scotch.io/@PratyushB/local-storage-vs-session-storage-vs-cookie)

Both these alternatives can also be encapsulated in a wrapper.

Of course server-side authentication and storage of the user's session details can also be implemented.

Also check out:

* [JWT](https://jwt.io/) (here's a [tutorial](https://jasonwatmore.com/post/2019/04/06/react-jwt-authentication-tutorial-example))
* [React Redux Session](https://www.npmjs.com/package/redux-react-session)
* [React Web Session](https://github.com/gilbarbara/react-web-session)