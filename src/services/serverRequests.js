import axios from 'axios';

export { performCalculation, htmlFromServer };

const url = "http://localhost:8080/calc/";

function performCalculation(type, payload) {
	return axios.post(url + type, payload)
	.then(function (response) {
		return response.data;
	})
	.catch(function (error) {
		console.log(error);
	});
}

function htmlFromServer() {
	return axios.get(url + "html")
	.then(function (response) {
		return response.data;
	})
	.catch(function (error) {
		console.log(error);
	});
}