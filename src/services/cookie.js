import Cookies from 'universal-cookie';

export {
	setCookie,
	getCookie,
	getAll,
	remove,
	addChangeListener,
	removeChangeListener
};

const cookies = new Cookies();

function setCookie(key, value) {
	cookies.set(key, value);
}

function getCookie(key) {
	return cookies.get(key);
}


function getAll() {
	return cookies.getAll();
}


function remove(key) {
	cookies.remove(key);
}

//adds listener to when a cookie is set or removed
function addChangeListener(callback) {
	cookies.addChangeListener(callback);
}

//removes a listener from the change callback
function removeChangeListener(callback) {
	cookies.removeChangeListener(callback);
}