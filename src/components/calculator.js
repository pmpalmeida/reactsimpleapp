import React, { Component } from 'react';
import CustomButton from './customButton';
import Display from './display';
import Temp from './testComponent';
import { performCalculation, htmlFromServer } from '../services/serverRequests';
import { setCookie } from '../services/cookie';
import { setItem } from '../services/storage';

import '../styles/calculator.css';

const firstRow = ['7','8','9'];
const secondRow = ['4','5','6'];
const thirdRow = ['1','2','3'];

class Calculator extends Component {
	clickButton = this.clickButton.bind(this);
	clearAll = this.clearAll.bind(this);
	addNumber = this.addNumber.bind(this);

	state = {
		'display': "_",
		'replace': true,
		'isFirstNumberDone': false,
		'selectedOp': "",
		'firstNumber': "",
		'secondNumber': "",
		'showTemp': false,
		'gotResponse': false,
		'response': ""
	};

	clickButton(text) {
		if(this.state.replace) {
			this.setState({
				'display': text,
				'replace': false
			});
		} else {
			this.setState({
				'display': this.state.display + text
			});
		}
	}

	doCalc() {
		if(this.state.selectedOp !== "") {
			let self = this;

			performCalculation(this.state.selectedOp, {
				'number1': this.state.firstNumber,
				'number2': this.state.secondNumber
			}).then(data => {
				self.setState({
					'display': data.result
				});
				this.clearValues();
			});
		}
	}

	addNumber(number) {
		if(this.state.isFirstNumberDone) {
			this.setState({
				'secondNumber': this.state.secondNumber + number
			});
		} else {
			if(this.state.replace) {
				this.setState({
					'firstNumber': number
				});
			} else {
				this.setState({
					'firstNumber': this.state.firstNumber + number
				});
			}
		}
		this.clickButton(number);
	}

	setOp(url, simbol) {
		if(this.state.selectedOp === "") {
			this.setState({
				'selectedOp': url,
				'isFirstNumberDone': true
			});
			this.clickButton(simbol);
		}
	}

	clearValues() {
		this.setState({
			'replace': true,
			'isFirstNumberDone': false,
			'selectedOp': "",
			'firstNumber': "",
			'secondNumber': ""
		});
	}

	clearAll() {
		this.clearValues();
		this.setState({
			'display': "_"
		});
	}

	tempButton() {
		this.setState({
			'showTemp': !this.state.showTemp
		});
	}

	setOnCookie() {
		setCookie("Display", this.state.display);
	}

	setOnStorage() {
		setItem("Display", this.state.display);
	}

	htmlFromServerTest() {
		let self = this;

		htmlFromServer()
		.then(data => {
			self.setState({
				'gotResponse': true,
				'response': data
			});
		});
	}

	treatInnerHtml() {
		return {
			'__html': this.state.response
		}
	}

	render() {
		return(
			<div className="container">
				<div className="display-container">
					<Display equation={this.state.display}/>
				</div>
				<div className="input-container number-container calc-container">
					<CustomButton handleClick={() => this.htmlFromServerTest()} text="OFF"/>
					<CustomButton handleClick={() => this.setOnStorage()} text="LOC"/>
					<CustomButton handleClick={() => this.setOnCookie()} text="CK"/>
					<CustomButton css="ac-container" handleClick={() => this.clearAll()} text="AC"/>

					{firstRow.map(number =>
						<CustomButton key={number} handleClick={() => this.addNumber(number)} text={number}/>
					)}
					<CustomButton handleClick={() => this.setOp("add", "+")} text="+"/>

					{secondRow.map(number =>
						<CustomButton key={number} handleClick={() => this.addNumber(number)} text={number}/>
					)}
					<CustomButton handleClick={() => this.setOp("minus", "-")} text="-"/>

					{thirdRow.map(number =>
						<CustomButton key={number} handleClick={() => this.addNumber(number)} text={number}/>
					)}
					<CustomButton handleClick={() => this.setOp("mult", "*")} text="*"/>

					<CustomButton handleClick={() => this.addNumber("0")} text="0"/>
					<CustomButton handleClick={() => {}} text="."/>
					<CustomButton handleClick={() => this.doCalc()} text="="/>
					<CustomButton handleClick={() => this.setOp("div", "/")} text="/"/>

				</div>
				{this.state.gotResponse && <div dangerouslySetInnerHTML={this.treatInnerHtml()}></div>}
			</div>
		);
	}
}

export default Calculator;