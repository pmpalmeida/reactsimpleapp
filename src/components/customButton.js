import React, { Component } from 'react';

import '../styles/customButton.css';

class CustomButton extends Component {
	state = {
		'text': this.props.text
	};

	render() {
		let text = this.state.text;

		if(this.props.css) {
			return(
				<button className={this.props.css} onClick={this.props.handleClick}>{text}</button>
			);
		} else {
			return(
				<button onClick={this.props.handleClick}>{text}</button>
			);
		}
	}
}

export default CustomButton;