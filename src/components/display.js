import React, { Component } from 'react';

import '../styles/display.css';

class Display extends Component {
	render() {
		return(
			<p className="display">{ this.props.equation }</p>
		);
	}
}

export default Display;