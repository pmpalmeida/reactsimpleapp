import React from 'react';
import './App.css';
import Calculator from './components/calculator';

function App() {

  const title = "R.E.S.T. Calculator"

  return (
    <div style={{'textAlign': 'center'}}>
      <header>
        <h1>
          Welcome to { title }!
        </h1>
      </header>
      <Calculator></Calculator>
    </div>
  );
}

export default App;
